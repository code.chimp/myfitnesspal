
# RESTful chat service


## Decisions

The language? Java.  Its my strongest language.
What REST framework would be
best or easiest to use?   To that end I chose Dropwizard because if its all
in one packaging - just three Dropwizard packages needed to compile and run.
It supplies an http server and object serialization.

The two issues that stuck out for me were how to store the data.   Given
longer for me to work on the project I would have the
cold storage be a no/sql database.   The primary key being unique message id assigned
to all incoming messages.

Lacking that as a proof of concept I went with two maps.  One for
the most recent list of messages keyed by username.   A second keyed by unique Id to message.  All
messages were placed in both on receipt necessitating only the removal of the messages with
get chat/{name} call from the current map.

This raised an interesting point.   With a single off Dropwizard storage service
it can act as a safe sequencer for all stores.   Using in memory hashmaps  are
not gated by default.

Two options.   An external storage singleton class with
appropriate locks to control access to the maps.   This synchronization is needed w/the
clearing out / adding messages the name map.   A ReantrantLock controls
this interaction.

What remains is where to put the singleton.   It turns out two, one for current requests, the other for cold requests.
A map in each for ID->Chat, and one NAME->Chat for current messages

## Limitations

There are several.   The most glaring is memory size.   The solution assumes all messages fit
in memory.   Which works until it doesn't.   Moving memory into a db pushes and increases storage
into a separate domain that is large than JVM heap.

There are copies of chats made when moving from current to cold storage.   Time.

This is a single web server solution.  There is no simple horizontally scalable extension for this solution.

## More Time
If I had more time the data stores would be in separate processes/machine(s) and the resource would not be singletons.

## Futures

In the future I would make cold storage a key/value lookup in SQL
This handles cold storage.

For current usage I could also put this into database, but would prefer
to keep this as fast as possible.    I would store the recent messages in a memory store w/storage like
Redis or Couchbase..

But with these backend changes in place we can then horizontally scale the web servers on the front. The path is open to
adding more servers on the back end

## Assumptions
What is the point of the timeout argument?   At no point does the spec say
what to do when that is exceeded.   Expiration_date.   Is this calculated
from timeout (now() + timeout) or is the time the message is moved to cold
storage?

I went with the latter.

## Building

### Prerequisites.
* Linux/OSX platform  to build and run on.
    Tested on mac osx.
* Java8 installed - https://www.java.com/en/download/
* Outside network access - for downloading build dependencies
* Latest Gradle installed  - https://gradle.org/install/
* curl tool to confirm setup - https://curl.haxx.se/download.html

### To build and run
Unzip/clone source into 'myfitnesspal' directory

git:  _git@gitlab.com:code.chimp/myfitnesspal.git_

$ cd myfitnesspal

From here

$ gradle build shadowJar

The executable jar is now located in myfitnesspal/libs/myfitnesspal-1.0.SNAPSHOT-all.jar

To run:

$ java -jar myfitnesspal/libs/myfitnesspal-1.0.SNAPSHOT-all.jar server ./src/main/resources/mypal.yaml

The server listens on port 8080 on localhost.

$ curl http://localhost:8080/fitness

Returns 'ok' if all is setup correctly

### Endpoints

There are three endpoints required in the spec, the data is expected in the exact format
as the spec.   Some fields are excluded on responses.
```
chat:
{
    "id" : <integer>                // returned only,          , not returned on id queries
    "name" : <String>               // users name              , not returned on name queries
    "text" : <String>               // text of chat
    "expiration" : <String>         // ISO 8061 date format GMT, set on expired chats only
    "timeout" : <integer>           // timeout?                . never returned
}

ok:
{
    "id" : <integer>                // returned on successful posts
}

Status.CREATED is returned on successful posts.
Status.NO_CONTENT is returned on missing arguments or null lookups
```