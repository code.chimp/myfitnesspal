package org.roger.myfitnesspal;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.roger.myfitnesspal.health.TemplateHealthCheck;
import org.roger.myfitnesspal.config.MyFitnessPalConfiguration;
import org.roger.myfitnesspal.resources.MyFitnessPalResource;

public class MyFitnessPalApplication extends Application<MyFitnessPalConfiguration> {
    public static void main(String[] args) throws Exception {
        new MyFitnessPalApplication().run(args);
    }

    @Override
    public String getName() {
        return "myfitnesspal";
    }

    @Override
    public void initialize(Bootstrap<MyFitnessPalConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(MyFitnessPalConfiguration configuration,
                    Environment environment) {
        final MyFitnessPalResource resource = new MyFitnessPalResource();

        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);


        environment.jersey().register(resource);
    }

}
