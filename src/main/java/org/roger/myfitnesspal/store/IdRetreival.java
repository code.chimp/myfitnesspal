package org.roger.myfitnesspal.store;

import org.roger.myfitnesspal.api.Chat;

public interface IdRetreival {

    void storeById(Chat m);
    Chat fetchById(Long id);
}
