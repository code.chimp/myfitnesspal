package org.roger.myfitnesspal.store;

import org.roger.myfitnesspal.api.Chat;

import java.util.List;

public interface NameRetrieval {
    void storeByName(Chat m);
    List<Chat> fetchByName(String name);
}
