package org.roger.myfitnesspal.store;

import org.roger.myfitnesspal.api.Chat;

import java.util.concurrent.ConcurrentHashMap;

public class ColdStore implements IdRetreival {
    private static ColdStore ourInstance = new ColdStore();
    public static ColdStore getInstance() {
        return ourInstance;
    }

    private ConcurrentHashMap<Long, Chat> cold = new ConcurrentHashMap<>();

    private ColdStore() {
    }

    @Override
    public void storeById(Chat m) {
        if (m==null || m.getId() == null) {
            System.err.println("store by id null obj " + m);
            return;
        }
        cold.put(m.getId(), m);
    }

    @Override
    public Chat fetchById(Long id) {
        Chat c;
        if ((c = cold.get(id)) == null) {
            System.err.println("cold fetch  by id null obj " + c);
            return null;
        }

        Chat o = new Chat(c);
        o.setId(null);
        return o;
    }
}
