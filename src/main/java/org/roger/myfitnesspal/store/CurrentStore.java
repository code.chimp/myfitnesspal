package org.roger.myfitnesspal.store;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.roger.myfitnesspal.api.Chat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class CurrentStore implements IdRetreival, NameRetrieval {
    private static CurrentStore ourInstance = new CurrentStore();

    public static CurrentStore getInstance() {
        return ourInstance;
    }
    public static ColdStore cold = ColdStore.getInstance();

    // many msg to username
    private Multimap<String, Chat> currentName = LinkedListMultimap.create();
    private ConcurrentHashMap<Long, Chat> currentId = new ConcurrentHashMap<>();

    private ReentrantLock lock = new ReentrantLock();

    private CurrentStore() {
    }

    //
    @Override
    public void storeByName(Chat m) {
        currentName.put(m.getUsername(), m);
    }

    //
    @Override
    public void storeById(Chat m) {
        currentId.put(m.getId(), m);
    }

    // complexity owing we move and expire these at the same time we remove
    @Override
    public List<Chat> fetchByName(String name) {
        List<Chat> rets = null;
        try {
            lock.lock();

            // Returned yo caller
            rets = new ArrayList<>(currentName.get(name));

            // make copy and expire those, not the returns
            rets.
                stream().
                map(x->new Chat(x)).
                forEach(x-> {cold.storeById(x); x.setExpiration();});

            // remove user ids from ID map
            rets.stream().forEach(x->currentId.remove(x.getId()));

            // remove chats  from name map
            currentName.get(name).clear();

        } finally {
            lock.unlock();
        }
        return rets;
    }

    //
    @Override
    public Chat fetchById(Long id) {
        Chat c;
        if ((c = currentId.get(id)) == null) {
            System.err.println(("Hot Id null"));
            return null;
        }
        return c;
    }
}
