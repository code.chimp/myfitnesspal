package org.roger.myfitnesspal.api;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("ok")
public class ChatCreated {
    public ChatCreated() {}
    public ChatCreated(long id) { this.id = id;}
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
