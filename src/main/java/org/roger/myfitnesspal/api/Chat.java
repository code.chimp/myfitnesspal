package org.roger.myfitnesspal.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonRootName("chat")
public class Chat implements Serializable {
    static DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    public Chat() {
    }

    // copy
    public Chat(Chat c) {
        this.username = c.username;
        this.text = c.text;
        this.id = c.id;
        this.expiration_date = c.expiration_date;
    }

    public Chat(String name, String message) {
        this.username = name;
        this.text = message;
    }

    @JsonProperty
    private Long id;

    @NotNull
    @JsonProperty
    private String username;

    @JsonProperty
    private String text;


    @JsonProperty
    String expiration_date;


    public void setId(Long id) {
        this.id = id;
    }

    public void setUserame(String name) {
        this.username = name;
    }

    public void setText(String message) {
        this.text = message;
    }

    public void setTimeout(Integer timeout) {
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getText() {
        return text;
    }

    public void setExpiration() {
        expiration_date = fmt.print(DateTime.now());
    }
    public String getExpiration() {
        return expiration_date;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", username, text);
    }
}
