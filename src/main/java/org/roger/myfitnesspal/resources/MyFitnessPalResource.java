package org.roger.myfitnesspal.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import org.roger.myfitnesspal.api.Chat;
import org.roger.myfitnesspal.api.ChatCreated;
import org.roger.myfitnesspal.store.ColdStore;
import org.roger.myfitnesspal.store.CurrentStore;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Path("/fitness")
@Produces(MediaType.APPLICATION_JSON)
public class MyFitnessPalResource {

    private final AtomicLong counter;

    private ColdStore oldStore = ColdStore.getInstance();
    private CurrentStore hotStore = CurrentStore.getInstance();


    public MyFitnessPalResource() {
        this.counter = new AtomicLong();
    }

    @GET
    public Response ping() {
        return Response.ok("ok\n").build();
    }

    /**
     * gets messages from anywhere by id.
     *
     * @param sid
     * @return
     */
    @GET
    @Path("/chat/{id}")
    @Timed
    public Response getMyMessage(@PathParam("id") Optional<String> sid) {
        if (sid.isPresent()) {
            String id = sid.get();
            Long lookupId = Long.parseLong(id);

            Chat c = oldStore.fetchById(lookupId);
            if (c == null) {
                c = hotStore.fetchById(lookupId);
            }

            if (c != null) {
                c = new Chat(c);
                c.setId(null);

                return Response.
                        ok().
                        entity(c).
                        build();
            }
        }
        return Response.noContent().build();
    }

    /**
     * pulls my message(s) out of current returns them to the user.
     * moves current messages to cold and sets expiry
     *
     * @param name
     * @return
     */
    @GET
    @Path("/chats/{name}")
    @Timed
    public Response getMyMessages(@PathParam("name") Optional<String> name) {

        List<Chat> rets = null;
        if (name.isPresent()) {
            String lookupName = name.get();
            rets = hotStore.
                    fetchByName(lookupName).
                    stream().
                    map(x->{x.setUserame(null); return x;}).
                    collect(Collectors.toList());

            return Response.ok(rets).build();
        } else {
            return Response.noContent().build();
        }
    }

    @POST
    @Path("/chat")
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public Response incomingMsg(Optional<Chat> chat) {

        if (!chat.isPresent())
            return Response.noContent().build();

        Chat c = chat.get();
        c.setId(counter.incrementAndGet());

        hotStore.storeByName(c);
        hotStore.storeById(c);

        return Response.
                status(Response.Status.CREATED).
                entity(new ChatCreated(c.getId())).
                build();
    }
}
